//
//  WakeUpTime+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 23/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension WakeUpTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WakeUpTime> {
        return NSFetchRequest<WakeUpTime>(entityName: "WakeUpTime")
    }

    @NSManaged public var hoursAndMinute: Date?

}
