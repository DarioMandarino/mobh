//
//  MapViewController.swift
//  MOBH
//
//  Created by Dario Mandarino on 09/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class MapViewController: UIViewController{
    
    @IBOutlet weak var alarmMonster: UIButton!
    @IBOutlet weak var waterMonster: UIButton!
    @IBOutlet weak var breakfastMonster: UIButton!
    @IBOutlet weak var mirrorMonster: UIButton!
    @IBOutlet weak var chuck: UIButton!
    @IBOutlet weak var monsterUnderTheBed: UIButton!
    @IBOutlet weak var fago: UIButton!
    @IBOutlet weak var theOldPhone: UIButton!
    @IBOutlet weak var skeleton: UIButton!
    @IBOutlet weak var electricianMonster: UIButton!
    
    override func viewDidLoad() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let dateFA = DataHandler.fetchDateFA(context: context)
        
        alarmMonster.isEnabled = false
        waterMonster.isEnabled = false
        breakfastMonster.isEnabled = false
        mirrorMonster.isEnabled = false
        chuck.isEnabled = false
        monsterUnderTheBed.isEnabled = false
        fago.isEnabled = false
        theOldPhone.isEnabled = false
        skeleton.isEnabled = false
        electricianMonster.isEnabled = false
        
        if(daysBetween(start: dateFA.first!.date!, end: Date()) < 8){
            alarmMonster.isHidden = false
            waterMonster.isHidden = false
            breakfastMonster.isHidden = false
            alarmMonster.isEnabled = true
            waterMonster.isEnabled = true
            breakfastMonster.isEnabled = true
        }
        
        if(daysBetween(start: dateFA.first!.date!, end: Date()) > 8 && daysBetween(start: dateFA.first!.date!, end: Date()) < 15){
            alarmMonster.isHidden = false
            waterMonster.isHidden = false
            breakfastMonster.isHidden = false
            mirrorMonster.isHidden = false
            chuck.isHidden = false
            alarmMonster.isEnabled = true
            waterMonster.isEnabled = true
            breakfastMonster.isEnabled = true
        }
        
    }
    
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
}
