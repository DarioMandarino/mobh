//
//  EquipItemVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 08/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class EquipItemVC: UIViewController{
    
    var name = String()
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var wisLabel: UILabel!
    @IBOutlet weak var dexLabel: UILabel!
    @IBOutlet weak var strLabel: UILabel!
   
    override func viewDidLoad() {
        setViews()
        let dismissGestureRecognizer = UIPanGestureRecognizer(target:  self, action: #selector(dismissGestureRecognizerAction(_:)))
               self.view.addGestureRecognizer(dismissGestureRecognizer)
    }
    
    @objc func dismissGestureRecognizerAction(_ gesture:UIPanGestureRecognizer ) {
        if gesture.state == .ended {
            self.dismiss(animated:  true, completion:  nil)
        }
    }
    private func setViews(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let equipItems = DataHandler.fetchEquip(context: context)
        
        for item in equipItems {
            if(item.nameCD == name){
                itemImage.image = UIImage(named: item.nameCD!)
                itemNameLabel.text = item.nameCD!
                itemDescription.text = item.descriptionCD
                priceLabel.text = String(item.sellPriceCD)
                strLabel.text = "STR " + String(item.strCD)
                dexLabel.text = "DEX " + String(item.dexCD)
                wisLabel.text = "WIS " + String(item.wisCD)
            }
        }
    }
    
    
    
    
    @IBAction func removeButton(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let equipItems = DataHandler.fetchEquip(context: context)
        
        for item in equipItems{
            if(item.nameCD == name){
                DataHandler.insertInventoryItem(context: context, sellPriceCD: item.sellPriceCD, nameCD: item.nameCD!, descriptionCD: item.descriptionCD!, strCD: item.strCD, wisCD: item.wisCD, dexCD: item.dexCD, typeCD: item.typeCD!)
                DataHandler.deleteObject(item, onContext: context, andCommit: true)
            }
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "rimuovoEquipNotifica"), object: self)
        dismiss(animated: true)
    }
    
    @IBAction func sellTapped(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let equipItems = DataHandler.fetchEquip(context: context)
        let pg = DataHandler.fetchCharacter(context: context)
        
        for item in equipItems {
            if(item.nameCD == name){
                DataHandler.updateCharacter(context: context, name: pg.first!.nameCD!, character: pg.first!, sexPG: (pg.first?.sexCD)!, currentHp: pg.first!.currentHpCD, currentHexp: pg.first!.currentExpCD, maxExp: pg.first!.maxExpCD, maxHp: pg.first!.maxHpCD, lv: pg.first!.lvCD, money: pg.first!.moneyCD + item.sellPriceCD, str: pg.first!.strCD, dex: pg.first!.dexCD, wis: pg.first!.wisCD)
                DataHandler.insertShopItem(context: context, cost: item.sellPriceCD * 2, nameCD: item.nameCD!, descriptionCD: item.descriptionCD!, strCD: item.strCD, wisCD: item.wisCD, dexCD: item.dexCD, typeCD: item.typeCD!)
                DataHandler.deleteObject(item, onContext: context, andCommit: true)
            }
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "vendoEquipNotifica"), object: self)
        dismiss(animated: true)
    }
    
}
