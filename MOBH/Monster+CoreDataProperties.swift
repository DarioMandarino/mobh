//
//  Monster+CoreDataProperties.swift
//  MOBH
//
//  Created by Dario Mandarino on 01/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//
//

import Foundation
import CoreData


extension Monster {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Monster> {
        return NSFetchRequest<Monster>(entityName: "Monster")
    }

    @NSManaged public var currentHPCD: Int64
    @NSManaged public var descriptionCD: String?
    @NSManaged public var idCD: Int16
    @NSManaged public var maxHPCD: Int64
    @NSManaged public var nameCD: String?
    @NSManaged public var dateGenerCD: Date

}
