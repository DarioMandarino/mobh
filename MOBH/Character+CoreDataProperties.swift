//
//  Character+CoreDataProperties.swift
//  MOBH
//
//  Created by Dario Mandarino on 05/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//
//

import Foundation
import CoreData


extension Character {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Character> {
        return NSFetchRequest<Character>(entityName: "Character")
    }

    @NSManaged public var currentExpCD: Int64
    @NSManaged public var currentHpCD: Int64
    @NSManaged public var lvCD: Int16
    @NSManaged public var maxExpCD: Int64
    @NSManaged public var maxHpCD: Int64
    @NSManaged public var nameCD: String?
    @NSManaged public var sexCD: String?
    @NSManaged public var moneyCD: Int16
    @NSManaged public var strCD: Int16
    @NSManaged public var dexCD: Int16
    @NSManaged public var wisCD: Int16

}
