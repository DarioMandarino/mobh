//
//  ChallengeViewController.swift
//  Reminders
//
//  Created by Dario Mandarino on 15/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit


class ChallengeViewController: UIViewController{
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var nameAndLevel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        
        let curHP = pg.first!.currentHpCD + Int64(pg.first!.dexCD)
        let maxHP = pg.first!.maxHpCD + Int64(pg.first!.dexCD)
        
        hpLabel.text = "HP: " + String(curHP) + "/" + String(maxHP)
    }
    
    
    override func viewDidLoad() {
        //Dobbiamo fare il fetch della settimana in cui ci troviamo
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        let dateFA = DataHandler.fetchDateFA(context: context)
        let checks = DataHandler.fetchTutorialChecks(context: context)
        
        let idFirstWeek = 1
        let altezza1 = Int(view.frame.height / 4) * 3 + 24
        //let idSecondWeek = 2
        var found = false
        var updated = false
        
        let curHP = pg.first!.currentHpCD + Int64(pg.first!.dexCD)
        let maxHP = pg.first!.maxHpCD + Int64(pg.first!.dexCD)
        
        nameAndLevel.text = pg.first!.nameCD! + " Lv:" + String(pg.first!.lvCD)
        hpLabel.text = "HP: " + String(curHP) + "/" + String(maxHP)
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
        
        if(daysBetween(start: (dateFA.first?.date)!, end: Date()) < 8){
            for check in checks{
                if(check.mobPresent == idFirstWeek){
                    found = true
                }
            }
            if(!found){
                for check in checks{
                    if(check.shopNewWeek == idFirstWeek){
                        DataHandler.updateCheck(context: context, tutorialCheck: check, shop: Int16(idFirstWeek), mob: Int16(idFirstWeek))
                        DataHandler.insertMonster(context: context, name: "Alarm Monster", currentHp: 70, maxHp: 70, id: 0, descr: "Set your alarm at 07:00",date: Date())
                        DataHandler.insertMonster(context: context, name: "Kraken", currentHp: 100, maxHp: 100, id: 1, descr: "Drink a glass of water",date: Date())
                        DataHandler.insertMonster(context: context, name: "Burgertron", currentHp: 50, maxHp: 50, id: 2, descr: "Eat healty breakfast",date: Date())
                        updated = true
                    }
                }
            }
            if(!updated && !found){
                //introduzione mostri prima settimana, magari picker
                DataHandler.insertCheck(context: context, shop: 1000, mob: Int16(idFirstWeek))
                DataHandler.insertMonster(context: context, name: "Alarm Monster", currentHp: 70, maxHp: 70, id: 0, descr: "Set your alarm at 07:00",date: Date())
                DataHandler.insertMonster(context: context, name: "Kraken", currentHp: 100, maxHp: 100, id: 1, descr: "Drink a glass of water",date: Date())
                DataHandler.insertMonster(context: context, name: "Burgertron", currentHp: 50, maxHp: 50, id: 2, descr: "Eat healty breakfast",date: Date())
            }
            //generazione task se siamo nella prima settimana
            
            let heightConstraint = NSLayoutConstraint(item: stack!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(altezza1))
            print("Metto il constraint: " + String(altezza1))
            
            stack.addConstraint(heightConstraint)
            generateMonsters()
        }
            
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 8) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 18)){
            
            secondWeek()
        }
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 15) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 21)){
            
            thirdWeek()
        }
        else if(daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 22){
            
            fourthWeek()
        }
        
        //L'altezza della stack va gestita dinamicamente, serve mettere un constraint per far funzionare la scroll
    }
    
    
    func generateMonsters(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let monsters = DataHandler.fetchMonsters(context: context)
        print("Monsters.count = " + String(monsters.count))
        var y = -30
         for monster in monsters{
            addMonster(Y: y,id: monster.idCD)
            y = y + Int(view.frame.height)/4
        }
    }
    
    func secondWeek(){
        
    }
    
    func thirdWeek(){
        
    }
    
    func fourthWeek(){
        
    }
    
    func addMonster(Y: Int,id: Int16){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        var found = false
        
        let monsters = DataHandler.fetchMonsters(context: context)
        print("TEST: id mostro = " + String(monsters[Int(id)].idCD) + ", passando come parametro id: " + String(id))
        let interactions = DataHandler.fetchInteractionPB(context: context)
       
        //questa è la view che deve essere aggiunta alla stack
        let view1 = UIView(frame: CGRect(x: 0,y: Y,width: Int(view.frame.width), height: Int(view.frame.height/4)))
        //prima di poterla aggiungere è il caso di inserire tutti gli elementi necessari
        //al bottone ho dovuto mettere un id per poter memorizzare quale è stato toccato, ho dovuto creare la classe PunchButton
        let button1 = PunchButton(frame: CGRect(x: view.frame.width * 0.0533, y: view.frame.height * 0.153, width: view.frame.width * 0.3467, height: view.frame.height * 0.1600))
        let hpLabel = UILabel(frame: CGRect(x: view.frame.width * 0.51, y: view.frame.height * 0.222, width: view.frame.width * 0.5893, height: view.frame.height * 0.0788))
        let title = UILabel(frame: CGRect(x: view.frame.width * 0.1893, y: view.frame.height * 0.0665, width: view.frame.width * 0.672, height: view.frame.height * 0.0813))
        let description = UILabel(frame: CGRect(x: view.frame.width * 0.51, y: view.frame.height * 0.1576, width: view.frame.width * 0.5893, height: view.frame.height * 0.0788))
        
        title.textAlignment = .center
        title.textColor = .black
        title.font = UIFont(name: "SF-Pro", size: 26)
        hpLabel.textColor = .black
        hpLabel.font = UIFont(name: "SF-Pro", size: 20)
        description.textColor = .black
        description.font = UIFont(name: "SF-Pro", size: 20)
       
        //se non è la prima volta che apro questa sezione il mostro sarà gia stato creato, lo fetcho con il suo stato attuale
        for monster in monsters{
            if(monster.idCD == id){
                hpLabel.text = "HP: " + String(monster.currentHPCD) + "/" + String(monster.maxHPCD)
                button1.setImage(UIImage(named: monster.nameCD!), for: .normal)
                button1.setImage(UIImage(named: monster.nameCD!), for: .highlighted)
                title.text = monster.nameCD
                description.text = monster.descriptionCD
            }
        }
        
        button1.setLalbel(label: hpLabel)
        button1.setID(ide: id)
        button1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        //devo verificare se sono state già fatte delle interazioni oggi e se ha fatto le challenge il giorno prima
        
        for inter in interactions{
            if(inter.idPBCD == id){
                found = true
                if(daysBetween(start: inter.datePunchCD!, end: Date()) == 0){
                    //oggi ha già picchiato il mostro
                    print("Second attempt today, button blocked")
                    button1.isEnabled = false
                }
                else if((daysBetween(start: inter.datePunchCD!, end: yesterday()) == 0)){
                    //ieri ha fatto la challenge, tutt appost
                    if(monsters[Int(id)].currentHPCD <= 0){
                        DataHandler.updateMonster(context: context, monster: monsters[Int(id)], name: monsters[Int(id)].nameCD!, currentHp: monsters[Int(id)].maxHPCD, maxHp: monsters[Int(id)].maxHPCD, id: monsters[Int(id)].idCD, descr: monsters[Int(id)].descriptionCD!,date: monsters[Int(id)].dateGenerCD )
                    }
                    print("Yesterday they have done the challenge")
                }
                else{
                    if(monsters[Int(id)].currentHPCD <= 0){
                        DataHandler.updateMonster(context: context, monster: monsters[Int(id)], name: monsters[Int(id)].nameCD!, currentHp: monsters[Int(id)].maxHPCD, maxHp: monsters[Int(id)].maxHPCD, id: monsters[Int(id)].idCD, descr: monsters[Int(id)].descriptionCD!,date: monsters[Int(id)].dateGenerCD)
                    }
                    //gestiamo come punirlo, non ha fatto la challenge e possiamo sapere anche per quanti giorni
                    //c'è stata un iterazione, ma non corrisponde ne a ieri ne a oggi
                }
            }
        }
        
        if(!found && daysBetween(start: Date(), end: monsters[Int(id)].dateGenerCD) != 0){
            print("QUesto non ha mai fatto sta challenge da quando è stata generata")
        }
                           
        view1.addSubview(hpLabel)
        view1.addSubview(button1)
        view1.addSubview(title)
        view1.addSubview(description)
        
        stack.addSubview(view1)
    }
    
    @objc func buttonAction(sender: PunchButton!) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        let monsters = DataHandler.fetchMonsters(context: context)
        let dmg = pg.first!.lvCD * 10 + pg.first!.strCD
        
        let strings1 = sender.getLalbel().text!.components(separatedBy: " ")
        let strings2 = strings1[1].components(separatedBy: "/")
        
        let currenthp = Int16(strings2.first!)! - dmg
        
        if(currenthp <= 0){
            for mob in monsters{
                if(mob.idCD == sender.getID()){
                    DataHandler.updateMonster(context: context, monster: mob, name: mob.nameCD!, currentHp: 0, maxHp: mob.maxHPCD, id: mob.idCD, descr: mob.descriptionCD!,date: mob.dateGenerCD)
                    DataHandler.updateCharacter(context: context,name: (pg.first?.nameCD)!,character: pg.first!,sexPG: (pg.first?.sexCD)!,currentHp: pg.first!.currentHpCD,currentHexp: pg.first!.currentExpCD,maxExp: pg.first!.maxExpCD,maxHp: pg.first!.maxHpCD,lv: pg.first!.lvCD,money: pg.first!.moneyCD + 200,str: pg.first!.strCD,dex: pg.first!.dexCD,wis: pg.first!.wisCD)
                    sender.getLalbel().text = "HP: 0" + "/" + strings2[1]
                }
            }
        }
        else{
            for mob in monsters{
                if(mob.idCD == sender.getID()){
                    DataHandler.updateMonster(context: context, monster: mob, name: mob.nameCD!, currentHp: Int64(currenthp), maxHp: mob.maxHPCD, id: mob.idCD, descr: mob.descriptionCD!,date: mob.dateGenerCD)
                    sender.getLalbel().text = "HP: " + String(currenthp) + "/" + strings2[1]
                }
            }
        }
        
        if(pg.first!.currentExpCD + 100 + Int64(pg.first!.wisCD) < pg.first!.maxExpCD){
            DataHandler.updateCharacter(context: context,name: (pg.first?.nameCD)!,character: pg.first!,sexPG: (pg.first?.sexCD)!,currentHp: pg.first!.currentHpCD,currentHexp: pg.first!.currentExpCD + 100 + Int64(pg.first!.wisCD),maxExp: pg.first!.maxExpCD,maxHp: pg.first!.maxHpCD,lv: pg.first!.lvCD,money: pg.first!.moneyCD,str: pg.first!.strCD,dex: pg.first!.dexCD,wis: pg.first!.wisCD)
        }
        else{
            let exp = pg.first!.currentExpCD + 100 + Int64(pg.first!.wisCD) - pg.first!.maxExpCD
            DataHandler.updateCharacter(context: context,name: (pg.first?.nameCD)!,character: pg.first!,sexPG: (pg.first?.sexCD)!,currentHp: pg.first!.currentHpCD + 100,currentHexp: exp,maxExp: pg.first!.maxExpCD + 500,maxHp: pg.first!.maxHpCD + 100,lv: pg.first!.lvCD + 1,money: pg.first!.moneyCD,str: pg.first!.strCD,dex: pg.first!.dexCD,wis: pg.first!.wisCD)
            nameAndLevel.text = pg.first!.nameCD! + " Lv:" + String(pg.first!.lvCD)
            
            let curHP = pg.first!.currentHpCD + Int64(pg.first!.dexCD)
            let maxHP = pg.first!.maxHpCD + Int64(pg.first!.dexCD)
            
            hpLabel.text = "HP: " + String(curHP) + "/" + String(maxHP)
        }
        
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
        sender.isEnabled = false
        
        let interactions = DataHandler.fetchInteractionPB(context: context)
        var flag = 0
        for inter in interactions{
            if(inter.idPBCD == sender.getID()){
                DataHandler.updateInteractionPB(context: context, interactionPB: inter, date: Date(), id: sender.getID())
                flag = 1
            }
        }
        
        if(flag == 0){
            DataHandler.insertInteractionPB(context: context, date: Date(), id: sender.getID())
        }
    }
    
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }

    func yesterday() -> Date {

       var dateComponents = DateComponents()
       dateComponents.setValue(-1, for: .day) // -1 day

       let now = Date() // Current date
       let yesterday = Calendar.current.date(byAdding: dateComponents, to: now) // Add the DateComponents

       return yesterday!
    }
   
}
