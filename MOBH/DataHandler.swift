

import Foundation
import CoreData
import UIKit

class DataHandler {
    
    static func saveData(onContext context: NSManagedObjectContext!){
        do {
            try context!.save()
        } catch {
            print("Failed saving")
        }
    }
    
    static func deleteObject(_ object: NSManagedObject, onContext context: NSManagedObjectContext!, andCommit: Bool){
        context.delete(object)
        if (andCommit){
            saveData(onContext: context)
        }
    }
    
    static func discardChanges(onContext context: NSManagedObjectContext!){
        context!.rollback()
    }
    
    /*
    static func allReminders() -> [Reminder] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return allRemindersOnContext(context)
    }
    
    static func allRemindersOnContext(_ context: NSManagedObjectContext) -> [Reminder] {
        let remindersFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        do {
            let reminders = try context.fetch(remindersFetchRequest)// as! [Reminder]
            return reminders as! [Reminder]
        } catch {
            print("error at context.execute")
            return []
        }
    }
    */
    
    static func insertEquipItem(context: NSManagedObjectContext,sellPriceCD: Int16,nameCD: String ,descriptionCD: String,strCD: Int16,wisCD: Int16,dexCD: Int16, typeCD: String){
        let equipItem = EquipmentItem(context: context)
        equipItem.descriptionCD = descriptionCD
        equipItem.dexCD = dexCD
        equipItem.nameCD = nameCD
        equipItem.sellPriceCD = sellPriceCD
        equipItem.strCD = strCD
        equipItem.typeCD = typeCD
        equipItem.wisCD = wisCD
        DataHandler.saveData(onContext: context)
    }
    
    static func updateEquipItem(context: NSManagedObjectContext,equipItem: EquipmentItem,sellPriceCD: Int16,nameCD: String ,descriptionCD: String,strCD: Int16,wisCD: Int16,dexCD: Int16, typeCD: String){
        equipItem.descriptionCD = descriptionCD
        equipItem.dexCD = dexCD
        equipItem.nameCD = nameCD
        equipItem.sellPriceCD = sellPriceCD
        equipItem.strCD = strCD
        equipItem.typeCD = typeCD
        equipItem.wisCD = wisCD
        DataHandler.saveData(onContext: context)
    }
    
    static func insertInventoryItem(context: NSManagedObjectContext,sellPriceCD: Int16,nameCD: String ,descriptionCD: String,strCD: Int16,wisCD: Int16,dexCD: Int16, typeCD: String){
        let inventoryItem = InventoryItem(context: context)
        inventoryItem.descriptionCD = descriptionCD
        inventoryItem.dexCD = dexCD
        inventoryItem.nameCD = nameCD
        inventoryItem.sellPriceCD = sellPriceCD
        inventoryItem.strCD = strCD
        inventoryItem.typeCD = typeCD
        inventoryItem.wisCD = wisCD
        DataHandler.saveData(onContext: context)
    }
    
    static func updateInventoryItem(context: NSManagedObjectContext,inventoryItem: InventoryItem,sellPriceCD: Int16,nameCD: String ,descriptionCD: String,strCD: Int16,wisCD: Int16,dexCD: Int16, typeCD: String){
        inventoryItem.descriptionCD = descriptionCD
        inventoryItem.dexCD = dexCD
        inventoryItem.nameCD = nameCD
        inventoryItem.sellPriceCD = sellPriceCD
        inventoryItem.strCD = strCD
        inventoryItem.typeCD = typeCD
        inventoryItem.wisCD = wisCD
        DataHandler.saveData(onContext: context)
    }
    
    static func insertShopItem(context: NSManagedObjectContext,cost: Int16,nameCD: String ,descriptionCD: String,strCD: Int16,wisCD: Int16,dexCD: Int16, typeCD: String){
        let shopItem = ShopItem(context: context)
        shopItem.descriptionCD = descriptionCD
        shopItem.dexCD = dexCD
        shopItem.nameCD = nameCD
        shopItem.costCD = cost
        shopItem.strCD = strCD
        shopItem.typeCD = typeCD
        shopItem.wisCD = wisCD
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchEquip (context: NSManagedObjectContext) -> [EquipmentItem]{
        let fetchRequest: NSFetchRequest<EquipmentItem> = EquipmentItem.fetchRequest()
        do{
            let equipItems =  try context.fetch(fetchRequest)
            return equipItems
        } catch{
            print("Problema con il fetch del equip")
            return []
        }
    }
    
    static func fetchShopItems (context: NSManagedObjectContext) -> [ShopItem]{
        let fetchRequest: NSFetchRequest<ShopItem> = ShopItem.fetchRequest()
        do{
            let shopItems =  try context.fetch(fetchRequest)
            return shopItems
        } catch{
            print("Problema con il fetch dello shop")
            return []
        }
    }
    
    static func fetchInventoryItems (context: NSManagedObjectContext) -> [InventoryItem]{
        let fetchRequest: NSFetchRequest<InventoryItem> = InventoryItem.fetchRequest()
        do{
            let inventory =  try context.fetch(fetchRequest)
            return inventory
        } catch{
            print("Problema con il fetch dell'inventario")
            return []
        }
    }
    
    static func insertWakeUp(context: NSManagedObjectContext,date: Date){
        let wakeUp = WakeUpTime(context: context)
        wakeUp.hoursAndMinute = date
        DataHandler.saveData(onContext: context)
       }
    
    static func fetchWakeUp (context: NSManagedObjectContext) -> [WakeUpTime]{
        let fetchRequest: NSFetchRequest<WakeUpTime> = WakeUpTime.fetchRequest()
        do{
            let wakeUps =  try context.fetch(fetchRequest)
            return wakeUps
        } catch{
            print("Problema con il fetch della sveglia")
            return []
        }
    }
    
    static func insertDateFA(context: NSManagedObjectContext,date: Date){
        let dateFA = DateFirstAccess(context: context)
        dateFA.date = date
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchDateFA (context: NSManagedObjectContext) -> [DateFirstAccess]{
        let fetchRequest: NSFetchRequest<DateFirstAccess> = DateFirstAccess.fetchRequest()
        do{
            let dateFA =  try context.fetch(fetchRequest)
            return dateFA
        } catch{
            print("Problema con il fetch della dataFA")
            return []
        }
    }
    
    static func insertInteractionPB(context: NSManagedObjectContext,date: Date,id: Int16){
        let interactionPB = InteractionPB(context: context)
        interactionPB.datePunchCD = date
        interactionPB.idPBCD = id
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchInteractionPB (context: NSManagedObjectContext) -> [InteractionPB]{
        let fetchRequest: NSFetchRequest<InteractionPB> = InteractionPB.fetchRequest()
        do{
            let interactionPB =  try context.fetch(fetchRequest)
            return interactionPB
        } catch{
            print("Problema con il fetch della dataPB")
            return []
        }
    }
    
    
    static func updateInteractionPB(context: NSManagedObjectContext,interactionPB : InteractionPB,date : Date,id: Int16){
        interactionPB.datePunchCD = date
        interactionPB.idPBCD = id
        DataHandler.saveData(onContext: context)
    }
    
    static func insertCheck(context: NSManagedObjectContext,shop: Int16,mob: Int16){
        let tutorialCheck = Checks(context: context)
        tutorialCheck.mobPresent = mob
        tutorialCheck.shopNewWeek = shop
        DataHandler.saveData(onContext: context)
    }
    
    static func updateCheck(context: NSManagedObjectContext,tutorialCheck: Checks,shop: Int16,mob: Int16){
        tutorialCheck.mobPresent = mob
        tutorialCheck.shopNewWeek = shop
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchTutorialChecks (context: NSManagedObjectContext) -> [Checks]{
        let fetchRequest: NSFetchRequest<Checks> = Checks.fetchRequest()
        do{
            let tutorialChecks =  try context.fetch(fetchRequest)
            return tutorialChecks
        } catch{
            print("Problema con il fetch dei check")
            return []
        }
    }
    
    static func insertCharacter(context: NSManagedObjectContext,name: String,sexPG: String,currentHp: Int64,currentHexp: Int64,maxExp: Int64,maxHp: Int64,lv: Int16,money: Int16,str: Int16,dex: Int16,wis: Int16){
        let character = Character(context: context)
        character.sexCD = sexPG
        character.nameCD = name
        character.currentExpCD = currentHexp
        character.currentHpCD = currentHp
        character.maxHpCD = maxHp
        character.maxExpCD = maxExp
        character.lvCD = lv
        character.moneyCD = money
        character.wisCD = wis
        character.strCD = str
        character.dexCD = dex
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchCharacter (context: NSManagedObjectContext) -> [Character]{
        let fetchRequest: NSFetchRequest<Character> = Character.fetchRequest()
        do{
            let pg =  try context.fetch(fetchRequest)
            return pg
        } catch{
            print("Problema con il fetch del pg")
            return []
        }
    }
    
    static func updateCharacter(context: NSManagedObjectContext,name: String,character: Character,sexPG: String,currentHp: Int64,currentHexp: Int64,maxExp: Int64,maxHp: Int64,lv: Int16,money: Int16,str: Int16,dex: Int16,wis: Int16){
        character.sexCD = sexPG
        character.nameCD = name
        character.currentExpCD = currentHexp
        character.currentHpCD = currentHp
        character.maxHpCD = maxHp
        character.maxExpCD = maxExp
        character.lvCD = lv
        character.moneyCD = money
        character.wisCD = wis
        character.strCD = str
        character.dexCD = dex
        DataHandler.saveData(onContext: context)
    }
    
    static func insertMonster(context: NSManagedObjectContext,name: String,currentHp: Int64,maxHp: Int64,id: Int16,descr: String,date: Date){
        let monster = Monster(context: context)
        monster.currentHPCD = currentHp
        monster.descriptionCD = descr
        monster.idCD = id
        monster.maxHPCD = maxHp
        monster.nameCD = name
        monster.dateGenerCD = date
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchMonsters (context: NSManagedObjectContext) -> [Monster]{
           let fetchRequest: NSFetchRequest<Monster> = Monster.fetchRequest()
           do{
               let mobs =  try context.fetch(fetchRequest)
               return mobs
           } catch{
               print("Problema con il fetch dei mostri")
               return []
           }
       }
    
    static func updateMonster(context: NSManagedObjectContext,monster : Monster,name: String,currentHp: Int64,maxHp: Int64,id: Int16,descr: String,date: Date){
        monster.currentHPCD = currentHp
        monster.descriptionCD = descr
        monster.idCD = id
        monster.maxHPCD = maxHp
        monster.nameCD = name
        monster.dateGenerCD = date
        DataHandler.saveData(onContext: context)
    }
}
