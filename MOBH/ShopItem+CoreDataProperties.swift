//
//  ShopItem+CoreDataProperties.swift
//  MOBH
//
//  Created by Dario Mandarino on 04/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//
//

import Foundation
import CoreData


extension ShopItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShopItem> {
        return NSFetchRequest<ShopItem>(entityName: "ShopItem")
    }

    @NSManaged public var descriptionCD: String?
    @NSManaged public var nameCD: String?
    @NSManaged public var strCD: Int16
    @NSManaged public var dexCD: Int16
    @NSManaged public var wisCD: Int16
    @NSManaged public var costCD: Int16
    @NSManaged public var typeCD: String?

}
