//
//  CollectionViewCell.swift
//  MOBH
//
//  Created by Dario Mandarino on 04/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageInsideCell: UIImageView!
}
