//
//  DateFirstAccess+CoreDataProperties.swift
//  MOBH
//
//  Created by Dario Mandarino on 01/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//
//

import Foundation
import CoreData


extension DateFirstAccess {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DateFirstAccess> {
        return NSFetchRequest<DateFirstAccess>(entityName: "DateFirstAccess")
    }

    @NSManaged public var date: Date?

}
