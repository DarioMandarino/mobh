//
//  DateCheck.swift
//  Reminders
//
//  Created by Dario Mandarino on 17/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class NotificationRequestVC: UIViewController{
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        
    }
    
    
    @IBAction func checkButton(_ sender: Any) {
        NotificationHandler.requestNotificationAuthorization()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let dateFA = Date()
        DataHandler.insertDateFA(context: context, date: dateFA)
        
        self.performSegue(withIdentifier: "onBoardingEnding", sender: self)
    }
    
}
