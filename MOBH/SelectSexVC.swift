//
//  SelectSexVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 01/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class SelectSexVC: UIViewController{
    
    var id = Int()
    
    
    @IBAction func druidTapped(_ sender: Any) {
        id = 1
        self.performSegue(withIdentifier: "addName", sender: self)
    }
    
    
    @IBAction func archerTapped(_ sender: Any) {
        id = 2
        self.performSegue(withIdentifier: "addName", sender: self)
    }
    
    
    @IBAction func barbarianTapped(_ sender: Any) {
        id = 3
        self.performSegue(withIdentifier: "addName", sender: self)
    }
    
    
    @IBAction func wizardTapped(_ sender: Any) {
        id = 4
        self.performSegue(withIdentifier: "addName", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "addName" {
            if let nextViewController = segue.destination as? AddNameCharacterVC {
                nextViewController.idCharacter = id
            }
        }
    }
}
