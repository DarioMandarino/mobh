//
//  TempleVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 07/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class TempleVC: UIViewController{
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sentenceLabel: UILabel!
    @IBOutlet weak var pgImage: UIImageView!
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var lvLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        setPg()
    }
    
    
    private func sentenceOD() {
        
    }
    
    private func setPg(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        
        nameLabel.text = "NAME: " + pg.first!.nameCD!
        lvLabel.text = "LV: " + String(pg.first!.lvCD)
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
        pgImage.image = UIImage(named: pg.first!.sexCD!)
    }
}
