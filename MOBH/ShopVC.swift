//
//  ShopVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 05/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class ShopVC: UIViewController{
    
    var nameTapped = String()
    var observer: NSObjectProtocol?
    var observerVendoInventario: NSObjectProtocol?
    var observerVendoEquipaggio: NSObjectProtocol?
    var buySomething = false
    var constraint = NSLayoutConstraint()
    
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var stack: UIStackView!
    
    
    override func viewDidLoad() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let dateFA = DataHandler.fetchDateFA(context: context)
        let checks = DataHandler.fetchTutorialChecks(context: context)
        let pg = DataHandler.fetchCharacter(context: context)
        let idFirstWeek = 1
        //        let idSecondWeek = 2
        //let idThirdWeek = 3
        var found = false
        var updated = false
        
        print("Aggiorno la label dei soldi")
        moneyLabel.text = String(pg.first!.moneyCD)
        
        if(daysBetween(start: (dateFA.first?.date)!, end: Date()) < 8){
            for check in checks{
                if(check.shopNewWeek == idFirstWeek){
                    found = true
                }
            }
            if(!found){
                for check in checks{
                    if(check.mobPresent == idFirstWeek){
                        print("Aggiorno il check per lo shop")
                        DataHandler.updateCheck(context: context, tutorialCheck: check, shop: Int16(idFirstWeek), mob: check.mobPresent)
                        DataHandler.insertShopItem(context: context, cost: 100, nameCD: "hammer", descriptionCD: "Excellent impact damage, although many prefer a sword, remains a good weapon.", strCD: 20, wisCD: 0, dexCD: 0, typeCD: "weapon")
                        DataHandler.insertShopItem(context: context, cost: 100, nameCD: "bow", descriptionCD: "Simple bow, but with an excellent range and good shooting power", strCD: 10, wisCD: 0, dexCD: 10, typeCD: "weapon")
                        DataHandler.insertShopItem(context: context, cost: 100, nameCD: "leather", descriptionCD: "Provides good defense against enemy attacs, but does not protect against their magic", strCD: 0, wisCD: 0, dexCD: 20, typeCD: "chest")
                        updated = true
                    }
                }
            }
            if(!updated && !found){
                print("Aggiungo da zero il check per lo shop")
                DataHandler.insertCheck(context: context, shop: Int16(idFirstWeek), mob: 1000)
                DataHandler.insertShopItem(context: context, cost: 100, nameCD: "hammer", descriptionCD: "Excellent impact damage, although many prefer a sword, remains a good weapon.", strCD: 20, wisCD: 0, dexCD: 0, typeCD: "weapon")
                DataHandler.insertShopItem(context: context, cost: 100, nameCD: "bow", descriptionCD: "Simple bow, but with an excellent range and good shooting power", strCD: 10, wisCD: 0, dexCD: 10, typeCD: "weapon")
                DataHandler.insertShopItem(context: context, cost: 100, nameCD: "leather", descriptionCD: "Provides good defense against enemy attacs, but does not protect against their magic", strCD: 0, wisCD: 0, dexCD: 20, typeCD: "chest")
            }
        }
            
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 8) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 18)){
            
        }
            
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 15) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 21)){
            
        }
        else if(daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 22){
            
        }
        generateItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        observer = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "buyNotifica"), object: nil, queue: .main){
            (notification) in
            print("Lo shop si accorge che è stato fatto un acquisto")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let pg = DataHandler.fetchCharacter(context: context)
            
            let popVC = notification.object as! BuyItemVC
            self.buySomething = popVC.buy
            if(self.buySomething){
                var i = 1
                for view1 in self.stack.subviews{
                    print("TOLGO LA VIEW NUMERO: " + String(i))
                    i = i + 1
                    view1.removeFromSuperview()
                }
                self.constraint.isActive = false
                self.moneyLabel.text = String(pg.first!.moneyCD)
                self.generateItems()
            }
        }
        observerVendoInventario = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "vendoInventarioNotifica"), object: nil, queue: .main){
        (notification) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let pg = DataHandler.fetchCharacter(context: context)
            print("Lo shop si accorge che è stato venduto qualcosa dall'inventario")
            var i = 1
            for view1 in self.stack.subviews{
                print("TOLGO LA VIEW NUMERO: " + String(i))
                i = i + 1
                view1.removeFromSuperview()
            }
            self.constraint.isActive = false
            self.moneyLabel.text = String(pg.first!.moneyCD)
            self.generateItems()
        }
        observerVendoEquipaggio = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "vendoEquipNotifica"), object: nil, queue: .main){
        (notification) in
            print("Lo shop si accorge che è stato venduto qualcosa dall'equip")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let pg = DataHandler.fetchCharacter(context: context)
            print("Lo shop si accorge che è stato venduto qualcosa dall'inventario")
            var i = 1
            for view1 in self.stack.subviews{
                print("TOLGO LA VIEW NUMERO: " + String(i))
                i = i + 1
                view1.removeFromSuperview()
            }
            self.constraint.isActive = false
            self.moneyLabel.text = String(pg.first!.moneyCD)
            self.generateItems()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let observer = observer {
        NotificationCenter.default.removeObserver(observer)
        }
    }
    
    func generateItems(){
        var y = 0
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let shopItems = DataHandler.fetchShopItems(context: context)
        print("VEDO: " + String(shopItems.count) + " oggetti")
        for item in shopItems{
            self.addItemToShop(name: item.nameCD!, Y: y)
            y = y + Int(self.view.frame.height)/6 + 20
        }
        //L'altezza della stack va gestita dinamicamente, serve mettere un constraint per far funzionare la scroll
        var altezza = CGFloat(integerLiteral: 0)
        for view1 in self.stack.subviews{
            altezza = altezza + view1.frame.height
        }
        //non so di preciso quanti elementi avrò nella mia stack: dipendono dalla settimana
        self.constraint = NSLayoutConstraint(item: self.stack!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: altezza)
        self.constraint.isActive = true
    }
    
    private func addItemToShop(name: String,Y: Int){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let shopItems = DataHandler.fetchShopItems(context: context)
        //questa è la view che deve essere aggiunta alla stack
        let view1 = UIView(frame: CGRect(x: 0,y: Y,width: Int(view.frame.width), height: Int(view.frame.height/6)))
        let line = UIImageView(frame: CGRect(x: 0, y: Int(0.0156 * view.frame.height), width: Int(view.frame.width), height: 1))
        let button = EquipmentButton(frame: CGRect(x: 0.0145 * view.frame.width, y: 0.0219 * view.frame.height, width: 0.31 * view.frame.width, height: 0.31 * view.frame.width))
        let nameLabel = UILabel(frame: CGRect(x: 0.4879 * view.frame.width, y: 0.018 * view.frame.height, width: 0.4275 * view.frame.width, height: 0.0356 * view.frame.height))
        let descriptionLabel = UILabel(frame: CGRect(x: 0.4299 * view.frame.width, y: 0.0546 * view.frame.height, width: 0.5314 * view.frame.width, height: 0.0831 * view.frame.height))
        let coinImage = UIImageView(frame: CGRect(x: 0.5894 * view.frame.width, y: 0.1449 * view.frame.height, width: 0.0676 * view.frame.width, height: 0.0676 * view.frame.width))
        let costLabel = UILabel(frame: CGRect(x: 0.6642 * view.frame.width, y: 0.1449 * view.frame.height, width: 0.0966 * view.frame.width, height: 0.0966 * view.frame.width))
        
        line.backgroundColor = .lightGray
        nameLabel.text = name
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.systemFont(ofSize: 14.0)
        descriptionLabel.numberOfLines = 5
        costLabel.textAlignment = .center
        costLabel.font = UIFont.systemFont(ofSize: 14.0)
        coinImage.image = UIImage(named: "coin")
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        for item in shopItems{
            if(item.nameCD == name){
                descriptionLabel.text = item.descriptionCD
                costLabel.text = String(item.costCD)
                button.setImage(UIImage(named: item.nameCD!), for: .normal)
                button.setImage(UIImage(named: item.nameCD!), for: .highlighted)
                button.nameCD = item.nameCD!
            }
        }
        
        view1.addSubview(line)
        view1.addSubview(button)
        view1.addSubview(nameLabel)
        view1.addSubview(descriptionLabel)
        view1.addSubview(coinImage)
        view1.addSubview(costLabel)
        
        stack.addSubview(view1)
        
    }
    
    @objc func buttonAction(sender: EquipmentButton!) {
        // cosa fare quando clicca su un oggetto dello shop
        nameTapped = sender.nameCD!
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let popup = sb.instantiateViewController(identifier: "buyVC") as BuyItemVC
        popup.name = nameTapped
        self.present(popup,animated: true)
    }
    
    private func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
}
