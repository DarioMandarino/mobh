//
//  InventoryVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 04/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class InventoryVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{
    
    @IBOutlet weak var pgImageView: UIImageView!
    
    var observerEquipaggio: NSObjectProtocol?
    var observerVendoInventario: NSObjectProtocol?
    var observerRimuovoEquipaggio: NSObjectProtocol?
    var observerVendoEquipaggio: NSObjectProtocol?
    
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
   
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var dexPoint: UILabel!
    @IBOutlet weak var strPoint: UILabel!
    @IBOutlet weak var wisPoint: UILabel!
    
    @IBOutlet weak var shield: EquipmentButton!
    @IBOutlet weak var helmet: EquipmentButton!
    @IBOutlet weak var weapon: EquipmentButton!
    @IBOutlet weak var chest: EquipmentButton!
    @IBOutlet weak var gem: EquipmentButton!
    @IBOutlet weak var boots: EquipmentButton!
    
    var nameTapped = String()
    var inventoryItems: [InventoryItem] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        //Qui entra solo la prima volta, settiamo gli oggetti con quello che abbiamo
        self.setUpInventory()
        self.setUpEquipment()
        self.setUpCharachter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("ENTRO NEL VIEWDIDAPPEAR DELL'INVENTARIO")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context).first
        
        let curHP = pg!.currentHpCD + Int64(pg!.dexCD)
        let maxHP = pg!.maxHpCD + Int64(pg!.dexCD)
        expLabel.text = String(pg!.currentExpCD) + "/" + String(pg!.maxExpCD)
        hpLabel.text = String(curHP) + "/" + String(maxHP)
        //Aggiorniamo l'inventario se è avvenuto un acquisto
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "buyNotifica"), object: nil, queue: .main){
        (notification) in
            print("L'inventario si accorge che è stato fatto un acquisto")
            self.setUpInventory()
            self.setUpCharachter()
        }
        observerEquipaggio = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "equipaggioNotifica"), object: nil, queue: .main){
        (notification) in
            print("L'inventario si accorge che è stato equipaggiato qualcosa")
            self.setUpInventory()
            self.setUpEquipment()
            self.setUpCharachter()
        }
        observerVendoInventario = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "vendoInventarioNotifica"), object: nil, queue: .main){
        (notification) in
            print("L'inventario si accorge che è stato venduto qualcosa")
            self.setUpInventory()
            self.setUpCharachter()
        }
        observerRimuovoEquipaggio = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "rimuovoEquipNotifica"), object: nil, queue: .main){
        (notification) in
            print("L'inventario si accorge che è stato rimosso qualcosa")
            self.setUpInventory()
            self.setUpEquipment()
            self.setUpCharachter()
        }
        observerVendoEquipaggio = NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "vendoEquipNotifica"), object: nil, queue: .main){
        (notification) in
            print("L'inventario si accorge che è stato venduto qualcosa dall'equip")
            self.setUpEquipment()
            self.setUpCharachter()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let observer1 = observerEquipaggio {
        NotificationCenter.default.removeObserver(observer1)
        }
        if let observer2 = observerVendoInventario {
        NotificationCenter.default.removeObserver(observer2)
        }
        if let observer3 = observerRimuovoEquipaggio {
        NotificationCenter.default.removeObserver(observer3)
        }
        if let observer4 = observerVendoEquipaggio {
        NotificationCenter.default.removeObserver(observer4)
        }
    }
    
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0,left: 32,bottom: 0,right: 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/2, height: self.collectionView.frame.size.height/3)
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inventoryItems.count  //items.count, tiene conto di quante celle deve creare
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        //cell.layer.borderColor = UIColor.lightGray.cgColor
        //cell.layer.borderWidth = 0.5
        cell.imageInsideCell.image = UIImage(named: inventoryItems[indexPath.item].nameCD!)
        //fare operazioni sulla cella, tipo settare immagini o altro,crea le celle
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        nameTapped = inventoryItems[indexPath.item].nameCD!
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let popup = sb.instantiateViewController(identifier: "inventoryItemVC") as InventoryItemVC
        popup.name = nameTapped
        self.present(popup,animated: true)
    }
    
    private func setUpInventory(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        inventoryItems = DataHandler.fetchInventoryItems(context: context)
        collectionView.reloadData()
        itemCountLabel.text = String(inventoryItems.count) + "/25"
    }
    
    private func setUpEquipment(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let equipment = DataHandler.fetchEquip(context: context)
        let pg = DataHandler.fetchCharacter(context: context).first
        
        var str = 0
        var dex = 0
        var wis = 0
        
        gem.isHidden = true
        gem.isEnabled = false
        
        weapon.isHidden = true
        weapon.isEnabled = false
        
        boots.isHidden = true
        boots.isEnabled = false
        
        shield.isHidden = true
        shield.isEnabled = false
        
        chest.isHidden = true
        chest.isEnabled = false
        
        helmet.isHidden = true
        helmet.isEnabled = false
        
        for equip in equipment{
            print("Nell equip ci sta: " + equip.nameCD!)
            str = str + Int(equip.strCD)
            wis = wis + Int(equip.wisCD)
            dex = dex + Int(equip.dexCD)
            
            if(equip.typeCD == "gem"){
                gem.nameCD = equip.nameCD!
                gem.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                gem.isEnabled = true
                gem.isHidden = false
            }
            else if(equip.typeCD == "boots"){
                boots.nameCD = equip.nameCD!
                boots.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                boots.isEnabled = true
                boots.isHidden = false
            }
            else if(equip.typeCD == "shield"){
                shield.nameCD = equip.nameCD!
                shield.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                shield.isEnabled = true
                shield.isHidden = false
            }
            else if(equip.typeCD == "weapon"){
                weapon.nameCD = equip.nameCD!
                weapon.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                weapon.isEnabled = true
                weapon.isHidden = false
            }
            else if(equip.typeCD == "chest"){
                chest.nameCD = equip.nameCD!
                chest.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                chest.isEnabled = true
                chest.isHidden = false
            }
            else if(equip.typeCD == "helmet"){
                helmet.nameCD = equip.nameCD!
                helmet.setBackgroundImage(UIImage(named: equip.nameCD!), for: .normal)
                helmet.isEnabled = true
                helmet.isHidden = false
            }
        }
        
        DataHandler.updateCharacter(context: context, name: pg!.nameCD!, character: pg!, sexPG: pg!.sexCD!, currentHp: pg!.currentHpCD, currentHexp: pg!.currentExpCD, maxExp: pg!.maxExpCD, maxHp: pg!.maxHpCD, lv: pg!.lvCD, money: pg!.moneyCD, str: Int16(str), dex: Int16(dex), wis: Int16(wis))
        
        wisPoint.text = String(pg!.wisCD)
        dexPoint.text = String(pg!.dexCD)
        strPoint.text = String(pg!.strCD)
        
        let curHP = pg!.currentHpCD + Int64(pg!.dexCD)
        let maxHP = pg!.maxHpCD + Int64(pg!.dexCD)
        hpLabel.text = String(curHP) + "/" + String(maxHP)
        expLabel.text = String(pg!.currentExpCD) + "/" + String(pg!.maxExpCD)
    }
    
    private func setUpCharachter(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        
        moneyLabel.text = String(pg.first!.moneyCD)
        nameLabel.text = String(pg.first!.nameCD!)
        pgImageView.image = UIImage(named: (pg.first?.sexCD)!)
    }
    
    @IBAction func equipButtonTapped(_ sender: EquipmentButton) {
        nameTapped = sender.nameCD!
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let popup = sb.instantiateViewController(identifier: "EquipItemVC") as EquipItemVC
        popup.name = nameTapped
        self.present(popup,animated: true)
    }
}
