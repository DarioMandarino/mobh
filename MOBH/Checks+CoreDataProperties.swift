//
//  Checks+CoreDataProperties.swift
//  MOBH
//
//  Created by Dario Mandarino on 05/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//
//

import Foundation
import CoreData


extension Checks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Checks> {
        return NSFetchRequest<Checks>(entityName: "Checks")
    }

    @NSManaged public var mobPresent: Int16
    @NSManaged public var shopNewWeek: Int16

}
