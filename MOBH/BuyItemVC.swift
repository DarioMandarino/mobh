//
//  BuyItemVC.swift
//  MOBH
//
//  Created by Dario Mandarino on 07/06/2020.
//  Copyright © 2020 iMastelloni. All rights reserved.
//

import Foundation
import UIKit

class BuyItemVC: UIViewController{
    
    var name = String()
    var buy = false
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var wisLabel: UILabel!
    @IBOutlet weak var dexLabel: UILabel!
    @IBOutlet weak var strLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    
    override func viewDidLoad() {
        setViews()
        let dismissGestureRecognizer = UIPanGestureRecognizer(target:  self, action: #selector(dismissGestureRecognizerAction(_:)))
        self.view.addGestureRecognizer(dismissGestureRecognizer)
    }
    @objc func dismissGestureRecognizerAction(_ gesture:UIPanGestureRecognizer ) {
        if gesture.state == .ended {
            self.dismiss(animated:  true, completion:  nil)
        }
    }
    
    
    private func setViews(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let shopItems = DataHandler.fetchShopItems(context: context)
        let pg = DataHandler.fetchCharacter(context: context)
        
        for item in shopItems {
            if(item.nameCD == name){
                itemImage.image = UIImage(named: item.nameCD!)
                itemNameLabel.text = item.nameCD!
                itemDescription.text = item.descriptionCD
                priceLabel.text = String(item.costCD)
                strLabel.text = "STR " + String(item.strCD)
                dexLabel.text = "DEX " + String(item.dexCD)
                wisLabel.text = "WIS " + String(item.wisCD)
                
                if(item.costCD > pg.first!.moneyCD){
                    buyButton.isEnabled = false
                }
            }
        }
    }
    
    
    @IBAction func buyTapped(_ sender: Any) {
        
        buy = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let shopItems = DataHandler.fetchShopItems(context: context)
        let pg = DataHandler.fetchCharacter(context: context)
        
        for item in shopItems{
            if(item.nameCD == name){
                DataHandler.updateCharacter(context: context, name: pg.first!.nameCD!, character: pg.first!, sexPG: (pg.first?.sexCD)!, currentHp: pg.first!.currentHpCD, currentHexp: pg.first!.currentExpCD, maxExp: pg.first!.maxExpCD, maxHp: pg.first!.maxHpCD, lv: pg.first!.lvCD, money: pg.first!.moneyCD - item.costCD, str: pg.first!.strCD, dex: pg.first!.dexCD, wis: pg.first!.wisCD)
                DataHandler.insertInventoryItem(context: context, sellPriceCD: item.costCD/2, nameCD: item.nameCD!, descriptionCD: item.descriptionCD!, strCD: item.strCD, wisCD: item.wisCD, dexCD: item.dexCD, typeCD: item.typeCD!)
                DataHandler.deleteObject(item, onContext: context, andCommit: true)
            }
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "buyNotifica"), object: self)
        dismiss(animated: true)
    }
}
