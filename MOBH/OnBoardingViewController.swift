//
//  OnboardingViewController.swift
//  Ritual
//
//  Created by Delia Cavalli on 05/05/2020.
//  Copyright © 2020 Delia Cavalli. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var enterButton: UIButton!
    
    var scrollWidth: CGFloat! = 0.0
    var scrollHeight: CGFloat! = 0.0
    
    var headings = ["Hello Stranger!","Everyone can lose their compass","Defeat monsters!"]
    var subheadings = ["I'm (name) the Samurai and I will accompany you on this adventure!","I'm here to help you set good habits and improve your well-being.","Taking on the monsters challenges will damage them: be steady and defeat them!"]
    
    //get dynamic width and height of scrollview and save it
    
    override func viewDidLayoutSubviews() {
        scrollWidth = scrollView.frame.size.width
        scrollHeight = scrollView.frame.size.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        //to call viewDidLayoutSubviews() and get dynamic width and height of scrollview
        enterButton.isEnabled = false
        self.scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        //crete the slides and add them
        
        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        for index in 0..<headings.count {
            frame.origin.x = scrollWidth * CGFloat(index)
            frame.size = CGSize(width: scrollWidth, height: scrollHeight)
            
            let slide = UIView(frame: frame)
            
            //subviews
            
            
            let imageView = UIImageView(frame: CGRect(x: 0.2826 * view.frame.width, y: 0.2790 * view.frame.height, width: 0.4347 * view.frame.width, height: 0.3806 * view.frame.height))
            imageView.image = UIImage(named: "pandaInArmatura1")
            imageView.contentMode = .scaleAspectFit
            
            let text1 = UILabel(frame: CGRect(x: 0.0241 * view.frame.width, y: 0.0614 * view.frame.height, width: 0.9541 * view.frame.width, height: 0.0324 * view.frame.height))
            text1.textAlignment = .center
            text1.font = UIFont.boldSystemFont(ofSize: 24.0)
            text1.text = headings[index]
            text1.textColor = .black
            text1.numberOfLines = 3
            
            let text2 = UILabel(frame: CGRect(x: 0.0676 * view.frame.width, y: 0.1161 * view.frame.height, width: 0.8000 * view.frame.width, height: 0.2000 * view.frame.height))
            text2.textAlignment = .center
            text2.numberOfLines = 5
            text2.font = UIFont.systemFont(ofSize: 20.0)
            text2.text = subheadings[index]
            text2.textColor = .black
            
            slide.addSubview(imageView)
            slide.addSubview(text1)
            slide.addSubview(text2)
            scrollView.addSubview(slide)
            
        }
        
        //set width of scrollview to accomodate all the slides
        
        scrollView.contentSize = CGSize(width: scrollWidth * CGFloat(headings.count), height: scrollHeight)
        
        //disable vertical scroll/bounce
        
        self.scrollView.contentSize.height = 1.0
        
        //initial state
        
        pageControl.numberOfPages = headings.count
        pageControl.currentPage = 0
        
    }
    
    //indicator
    
    @IBAction func pageChanged(_ sender: Any) {
        scrollView!.scrollRectToVisible(CGRect(x: scrollWidth * CGFloat ((pageControl?.currentPage)!), y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setIndiactorForCurrentPage()
    }
    
    func setIndiactorForCurrentPage()  {
        let page = (scrollView?.contentOffset.x)!/scrollWidth
        pageControl?.currentPage = Int(page)
        
        
        if(pageControl?.currentPage == 2){
            
            enterButton.isEnabled = true
        }
    }
    
    // Do any additional setup after loading the view.
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */



