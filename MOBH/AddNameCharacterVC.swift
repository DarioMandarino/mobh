//
//  AddNameCharacterVC.swift
//  Reminders
//
//  Created by Dario Mandarino on 18/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit

class AddNameCharacterVC: UIViewController{
    
    var idCharacter = Int()
    
    var sex = String()
    
    @IBOutlet weak var addNameTextField: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch idCharacter {
        case 1:
            imageView.image = UIImage(named: "Druid")
            sex = "Druid"
        case 2:
            imageView.image = UIImage(named: "Archer")
            sex = "Archer"
        case 3:
            imageView.image = UIImage(named: "Barbarian")
            sex = "Barbarian"
        case 4:
            imageView.image = UIImage(named: "Wizard")
            sex = "Wizard"
        default:
            print("Non è arrivato il dato")
        }
    }
    
    
    @IBAction func okTapped(_ sender: Any) {
        
        if(textField.text != ""){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
            DataHandler.insertCharacter(context: context, name: textField.text!, sexPG:  sex, currentHp: 100, currentHexp: 900, maxExp: 1000, maxHp: 100, lv: 1,money: 200,str: 0,dex: 0,wis: 0)
        performSegue(withIdentifier: "goToNotification", sender: self)
        }
        else{
            textField.text = "PLS write your name"
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        addNameTextField.resignFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNameTextField.delegate = self
    }
}

extension AddNameCharacterVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addNameTextField.resignFirstResponder()
        return true
    }
}
